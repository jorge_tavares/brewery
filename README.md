# Brewery Microservice

Este é o repositório do (micro) serviço **brewery**, responsável por cadastrar tipos de cerveja, seguida de sua temperatura mínima e máxima, sendo possível encontrar qual é o estilo ideal para a temperatura desejada =). Ah, e para que sua diversão fique ainda melhor, trouxemos, junto com a escolha ideal, uma playlist vinda diretamente do [Spotify](https://www.spotify.com/br/), para você ouvir e tomar sua cerveja tranquilamente! 

#O que tem de legal?

Neste projeto, foi utilizado as seguintes tecnologias:

- Java 8
- Spring Boot v2.0.5
- Lombok
- JUnit com Mockito
- Fixture Factory
- Jacoco
- Swagger Spring Fox
- H2 Database 
- Docker


**Observações**: 

- Neste projeto foi implementado o conceito de Package Scope, onde os pacotes são divididos por responsábilidade e cada pacote tem apenas **uma** interface pública e as implementações de serviço e persistência não podem ser acessadas por outro pacote (com excessão do core).

- A aplicação está com 22 testes unitários, não sendo possível atingir 100% de cobertura devido algumas _annotations_ do _framework_ Lombok. O _framework_ Jacoco está configurado para ter ao menos um teste unitário para cada caminho de um método.

- Foi realizado uma integração com a API do Spotify, podendo ser localizada no pacote `integration.spotify`

- A aplicação roda em um Spring Boot, logo, está rodando emcima de um Tomcat e foi utilizado a porta _default_ `8080` (localmemente)

- A aplicação está "deployada" no [Heroku](https://www.heroku.com/), podendo ser acessada pelo caminho: https://sleepy-reaches-71452.herokuapp.com/v1/beers/ (endpoint que expõe os estilos de cerveja cadastrados)

- Ao subir a aplicação, existe um código localizado no pacote `configuration` que popula a base de dados para facilizar o uso do _endpoint_ que encontra a cerveja ideal dado uma temperatura, abaixo tem uma tabela que representa os dados _Mock_ que são inseridos:

 |Estilo da Cerveja | Min e Máx Temperatura
 |Weissbier |-1° a 3°|
 |Pilsens |-2° a 4°|
 |Weizenbier |-4° a 6°|
 |Red ale |-5° a 5°|
 |India pale ale |-6° a 7°|
 |IPA |-7° a 10°|
 |Dunkel |-8° a 2°|
 |Imperial Stouts |-10° a 13°|
 |Brown ale |0° a 14°|


# Rodando a aplicação

Para rodar este microserviço devemos instalar as seguintes ferramentas:
    
- [Maven](http://maven.apache.org/install.html)
- [Docker](https://docs.docker.com/install/)
- [Docker Compose](https://docs.docker.com/compose/install/)

## Rodando no ambiente Linux

Após instalar as ferramentas necessárias, vá até a pasta raíz do projeto e execute o script `run.sh`, utilizando o seguinte comando `$ bash run.sh`

Obs: a ferramenta Docker deve estar configurada para rodar sem super usuário, para detalhes de como fazer, acesse este [link](https://docs.docker.com/install/linux/linux-postinstall/).


## Rodando no ambiente Windows

Após instalar as ferramentas necessárias, devemos rodar os seguintes comando: 

`mvn clean package docker:build` e `docker-compose up`

## Documentação da API

Com a aplicação em pé, acesse: [http://localhost:8080/swagger-ui.html#](http://localhost:8080/swagger-ui.html#) ou https://sleepy-reaches-71452.herokuapp.com/swagger-ui.html# . Abaixo tem um exemplo de como usar o _endpoint_ que traz o estilo cerveja ideal com a _playlist_ do Spotify:

Dado a temperatura desejada:

- Heroku (via curl): 

```
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '-2' 'https://sleepy-reaches-71452.herokuapp.com/v1/brewery-machines/ideals'
```

- Local: 

`POST em http://localhost:8080/v1/brewery-machines/ideals`

com o _body_ `application/json`

```json
{
  "temperature": -2
}
```

Temos a seguinte saída:

```json
{
  "beerStyle": "Dunkel",
  "playlist": {
    "self": "https://api.spotify.com/v1/search?query=Dunkel&type=playlist&offset=0&limit=1",
    "name": "matilda dunkel",
    "tracks": {
      "self": "https://api.spotify.com/v1/playlists/4kNUekYK7MqqSSNomS4n1w/tracks",
      "musicTotal": 1582
    }
  }
}
```

**Obs**: Foi alterado o corpo da saída por questões de quantidade de músicas elevada em determinadas _playlists_, logo foi utlizado o conceito de **HATEOS** (simples) para devolver o _link_ das _tracks_ para que um _client_ REST realize outra requisição e devolva os _links_ de cada músicas com paginação. Deixando o retorno das requisições mais enxuta.
