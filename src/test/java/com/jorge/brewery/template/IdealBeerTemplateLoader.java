package com.jorge.brewery.template;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.jorge.brewery.machine.IdealBeer;
import com.jorge.brewery.machine.IdealPlaylist;

import static com.jorge.brewery.template.FixtureTemplateType.IDEAL;
import static com.jorge.brewery.template.FixtureTemplateType.VALID;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
public class IdealBeerTemplateLoader implements TemplateLoader {
    @Override
    public void load() {
        Fixture.of(IdealBeer.class).addTemplate(IDEAL.name(), new Rule() {{
            add("beerStyle", "Dunkel");
            add("playlist", one(IdealPlaylist.class, IDEAL.name()));
        }});

    }
}
