package com.jorge.brewery.template;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.jorge.brewery.integration.spotify.SpotifyPlaylist;
import com.jorge.brewery.integration.spotify.SpotifyTracks;

import static com.jorge.brewery.template.FixtureTemplateType.IDEAL;
import static com.jorge.brewery.template.FixtureTemplateType.VALID;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
public class SpotifyTracksTemplateLoader implements TemplateLoader {
    @Override
    public void load() {
        Fixture.of(SpotifyTracks.class).addTemplate(IDEAL.name(), new Rule() {{
            add("href", "https://api.spotify.com/v1/playlists/4kNUekYK7MqqSSNomS4n1w/tracks");
            add("total", 1580);
        }});

    }
}
