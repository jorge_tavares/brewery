package com.jorge.brewery.template;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.jorge.brewery.integration.spotify.SpotifyPlaylistItem;
import com.jorge.brewery.integration.spotify.SpotifyTracks;

import static com.jorge.brewery.template.FixtureTemplateType.IDEAL;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
public class SpotifyPlaylistItemTemplateLoader implements TemplateLoader {
    @Override
    public void load() {
        Fixture.of(SpotifyPlaylistItem.class).addTemplate(IDEAL.name(), new Rule() {{
            add("name", "matilda dunkel");
            add("tracks", one(SpotifyTracks.class, IDEAL.name()));
        }});

    }
}
