package com.jorge.brewery.template;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.jorge.brewery.beer.Beer;

import java.util.Date;

import static com.jorge.brewery.template.FixtureTemplateType.IDEAL;
import static com.jorge.brewery.template.FixtureTemplateType.MODIFIED;
import static com.jorge.brewery.template.FixtureTemplateType.VALID;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
public class BeerTemplateLoader implements TemplateLoader {
    @Override
    public void load() {
        Fixture.of(Beer.class).addTemplate(VALID.name(), new Rule() {{
            add("id", random(Integer.class, range(1, 200)));
            add("createdAt", new Date());
            add("lastModified", new Date());
            add("style", random("Weissbier", "Pilsens", "Weizenbier", "Red ale", "IPA", "Dunkel", "Imperial Stouts", "Brown ale"));
            add("minTemperature", random(Integer.class, range(-10, 0)));
            add("maxTemperature", random(Integer.class, range(0, 14)));
            add("avgTemperature", random(Integer.class, range(-10, 14)));
        }});

        Fixture.of(Beer.class).addTemplate(IDEAL.name()).inherits(VALID.name(), new Rule() {{
            add("style", "Dunkel");
            add("minTemperature", -8);
            add("maxTemperature", 2);
            add("avgTemperature", -3);
        }});

        Fixture.of(Beer.class).addTemplate(MODIFIED.name()).inherits(VALID.name(), new Rule() {{
            add("style", "India Pale Ale");
        }});
    }
}
