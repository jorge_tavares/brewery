package com.jorge.brewery.template;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
public enum FixtureTemplateType {
    VALID,
    IDEAL,
    MODIFIED
}
