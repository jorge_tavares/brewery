package com.jorge.brewery.template;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.jorge.brewery.machine.IdealTracks;

import static com.jorge.brewery.template.FixtureTemplateType.IDEAL;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
public class IdealTracksTemplateLoader implements TemplateLoader {

    @Override
    public void load() {
        Fixture.of(IdealTracks.class).addTemplate(IDEAL.name(), new Rule() {{
            add("self", "https://api.spotify.com/v1/playlists/4kNUekYK7MqqSSNomS4n1w/tracks");
            add("musicTotal", 1581);
        }});
    }
}
