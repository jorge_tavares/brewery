package com.jorge.brewery.template;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.jorge.brewery.integration.spotify.SpotifyPlaylist;
import com.jorge.brewery.integration.spotify.SpotifyPlaylistItem;

import static com.jorge.brewery.template.FixtureTemplateType.IDEAL;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
public class SpotifyPlaylistTemplateLoader implements TemplateLoader {
    @Override
    public void load() {
        Fixture.of(SpotifyPlaylist.class).addTemplate(IDEAL.name(), new Rule() {{
            add("href", "https://api.spotify.com/v1/search?query=%22Dunkel%22&type=playlist&market=BR&offset=0&limit=1");
            add("items", has(1).of(SpotifyPlaylistItem.class, IDEAL.name()));
        }});
    }
}
