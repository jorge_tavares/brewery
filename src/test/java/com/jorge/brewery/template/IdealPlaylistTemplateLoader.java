package com.jorge.brewery.template;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.jorge.brewery.machine.IdealPlaylist;
import com.jorge.brewery.machine.IdealTracks;

import static com.jorge.brewery.template.FixtureTemplateType.IDEAL;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
public class IdealPlaylistTemplateLoader implements TemplateLoader {

    @Override
    public void load() {
        Fixture.of(IdealPlaylist.class).addTemplate(IDEAL.name(), new Rule() {{
            add("self", "https://api.spotify.com/v1/search?query=Dunkel&type=playlist&offset=0&limit=1");
            add("name", "matilda dunkel");
            add("tracks", one(IdealTracks.class, IDEAL.name()));
        }});
    }
}
