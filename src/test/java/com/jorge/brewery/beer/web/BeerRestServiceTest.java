package com.jorge.brewery.beer.web;

import br.com.six2six.fixturefactory.Fixture;
import com.google.common.collect.Lists;
import com.jorge.brewery.beer.Beer;
import com.jorge.brewery.beer.BeerService;
import com.jorge.brewery.core.AbstractTest;
import com.jorge.brewery.template.FixtureTemplateType;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;

import static com.jorge.brewery.template.FixtureTemplateType.MODIFIED;
import static com.jorge.brewery.template.FixtureTemplateType.VALID;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

/**
 * Jorge Tavares jorge.tavares.inatel@gmail.com
 */
public class BeerRestServiceTest extends AbstractTest {

    @Mock
    private BeerService beerService;

    private BeerRestService beerRestService;

    @Override
    public void init() {
        beerRestService = new BeerRestService(beerService);
    }

    @Test
    public void should_create_a_new_from_api() {
        Beer beer = Fixture.from(Beer.class).gimme(VALID.name());

        when(beerService.create(beer)).thenReturn(beer);

        Assert.assertEquals(beer, beerRestService.create(beer));

        InOrder inOrder = inOrder(beerService);
        inOrder.verify(beerService, times(1)).create(beer);
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void should_update_an_existent() {
        Beer beer = Fixture.from(Beer.class).gimme(VALID.name());
        Beer modified = Fixture.from(Beer.class).gimme(MODIFIED.name());

        when(beerService.update(beer.getId(), beer)).thenReturn(modified);

        Assert.assertEquals(modified, beerRestService.update(beer.getId(), beer));

        InOrder inOrder = inOrder(beerService);
        inOrder.verify(beerService, times(1)).update(beer.getId(), beer);
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void should_delete_an_existent() {
        beerRestService.delete(10);
        InOrder inOrder = inOrder(beerService);
        inOrder.verify(beerService, times(1)).delete(10);
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void should_retrieve_just_one_from_api() {
        Beer beer = Fixture.from(Beer.class).gimme(VALID.name());
        final Integer id = beer.getId();

        when(beerService.findOne(any(Specification.class))).thenReturn(beer);

        Assert.assertEquals(beer, beerRestService.findOne(id));

        InOrder inOrder = inOrder(beerService);
        inOrder.verify(beerService, times(1)).findOne(any(Specification.class));
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void should_retrieve_the_first_page_from_api() {
        final List<Beer> beers = Lists.newArrayList(Fixture.from(Beer.class).gimme(5, VALID.name()));
        final Page<Beer> firstPage = new PageImpl<>(beers);

        when(beerService.findAll(any(Pageable.class))).thenReturn(firstPage);

        Assert.assertEquals(firstPage, beerRestService.findAll(1, 20));

        InOrder inOrder = inOrder(beerService);
        inOrder.verify(beerService, times(1)).findAll(any(Pageable.class));
        inOrder.verifyNoMoreInteractions();
    }
}