package com.jorge.brewery.beer;

import br.com.six2six.fixturefactory.Fixture;
import com.jorge.brewery.core.AbstractTest;
import com.jorge.brewery.core.exception.AlreadyExistsException;
import com.jorge.brewery.core.exception.ResourceNotFoundException;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

import static com.jorge.brewery.beer.BeerSpecification.id;
import static com.jorge.brewery.template.FixtureTemplateType.IDEAL;
import static com.jorge.brewery.template.FixtureTemplateType.MODIFIED;
import static com.jorge.brewery.template.FixtureTemplateType.VALID;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * Jorge Tavares jorge.tavares.inatel@gmail.com
 */
public class BeerServiceImplTest extends AbstractTest {

    @Mock
    private BeerRepository beerRepository;

    private BeerService beerService;

    @Override
    public void init() {
        beerService = new BeerServiceImpl(beerRepository);
    }

    @Test
    public void should_create_a_new() {
        final Beer beer = Fixture.from(Beer.class).gimme(VALID.name());

        when(beerRepository.findOne(any(Specification.class))).thenReturn(empty());
        when(beerRepository.save(beer)).thenReturn(beer);

        assertEquals(beer.getId(), beerService.create(beer).getId());

        InOrder inOrder = inOrder(beerRepository);
        inOrder.verify(beerRepository, Mockito.times(1)).findOne(any(Specification.class));
        inOrder.verify(beerRepository, Mockito.times(1)).save(beer);
        inOrder.verifyNoMoreInteractions();
    }

    @Test(expected = AlreadyExistsException.class)
    public void should_throw_a_already_exists_exception_when_the_beer_is_present() {
        final Beer beer = Fixture.from(Beer.class).gimme(VALID.name());

        when(beerRepository.findOne(any(Specification.class))).thenReturn(of(beer));

        beerService.create(beer);

        InOrder inOrder = inOrder(beerRepository);
        inOrder.verify(beerRepository, Mockito.times(1)).findOne(any(Specification.class));
    }

    @Test
    public void should_update_an_existent() {
        Beer beer = Fixture.from(Beer.class).gimme(VALID.name());
        Beer modified = Fixture.from(Beer.class).gimme(MODIFIED.name());

        when(beerRepository.findOne(any(Specification.class))).thenReturn(of(beer));
        when(beerRepository.save(modified)).thenReturn(modified);

        assertEquals(modified, beerService.update(beer.getId(), modified));

        InOrder inOrder = inOrder(beerRepository);
        inOrder.verify(beerRepository, Mockito.times(1)).findOne(any(Specification.class));
        inOrder.verify(beerRepository, Mockito.times(1)).save(modified);
        inOrder.verifyNoMoreInteractions();
    }

    @Test(expected = ResourceNotFoundException.class)
    public void should_thrown_a_resource_not_found_when_tries_to_update_an_not_existent() {
        final Beer beer = Fixture.from(Beer.class).gimme(VALID.name());

        when(beerRepository.findOne(any(Specification.class))).thenReturn(empty());

        beerService.update(beer.getId(), beer);

        InOrder inOrder = inOrder(beerRepository);
        inOrder.verify(beerRepository, Mockito.times(1)).findOne(any(Specification.class));
    }

    @Test
    public void should_retrieve_first_page() {
        final List<Beer> beers = Fixture.from(Beer.class).gimme(2, VALID.name());
        final PageImpl<Beer> beerPage = new PageImpl<>(beers);

        when(beerRepository.findAll(any(Pageable.class))).thenReturn(beerPage);

        Assert.assertEquals(beerPage, beerService.findAll(new PageRequest(0, 20)));

        InOrder inOrder = inOrder(beerRepository);
        inOrder.verify(beerRepository, times(1)).findAll(any(Pageable.class));
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void should_retrieve_just_one() {
        final Beer beer = Fixture.from(Beer.class).gimme(VALID.name());

        when(beerRepository.findOne(any(Specification.class))).thenReturn(of(beer));

        assertEquals(beer, beerService.findOne(id(anyInt())));

        InOrder inOrder = inOrder(beerRepository);
        inOrder.verify(beerRepository, times(1)).findOne(any(Specification.class));
        inOrder.verifyNoMoreInteractions();
    }

    @Test(expected = ResourceNotFoundException.class)
    public void should_thrown_a_resource_not_found_when_beer_not_exists() {
        when(beerRepository.findOne(any(Specification.class))).thenReturn(empty());

        beerService.findOne(id(anyInt()));

        InOrder inOrder = inOrder(beerRepository);
        inOrder.verify(beerRepository, times(1)).findOne(any(Specification.class));
    }

    @Test
    public void should_delete() {
        final Beer beer = Fixture.from(Beer.class).gimme(VALID.name());

        when(beerRepository.findOne(any(Specification.class))).thenReturn(of(beer));

        beerService.delete(beer.getId());

        InOrder inOrder = inOrder(beerRepository);
        inOrder.verify(beerRepository, times(1)).findOne(any(Specification.class));
        inOrder.verify(beerRepository, times(1)).delete(beer);
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void should_retrieve_a_ideal_beer_by_temperature() {
        final Beer dunkel = Fixture.from(Beer.class).gimme(IDEAL.name());

        when(beerRepository.findIdeal(-2)).thenReturn(of(dunkel));

        Assert.assertEquals(of(dunkel), beerRepository.findIdeal(-2));

        InOrder inOrder = inOrder(beerRepository);
        inOrder.verify(beerRepository, times(1)).findIdeal(-2);
        inOrder.verifyNoMoreInteractions();
    }

    @Test(expected = ResourceNotFoundException.class)
    public void should_throw_a_resource_not_found_exception_when_does_not_have_beers_registered() {
        when(beerRepository.findIdeal(anyInt())).thenReturn(empty());

        beerService.findIdeal(5);

        InOrder inOrder = inOrder(beerRepository);
        inOrder.verify(beerRepository, times(1)).findIdeal(anyInt());
    }
}