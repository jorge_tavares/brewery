package com.jorge.brewery.integration.spotify;

import br.com.six2six.fixturefactory.Fixture;
import com.jorge.brewery.core.AbstractTest;
import com.jorge.brewery.core.exception.IntegrationException;
import com.jorge.brewery.core.properties.IntegrationSpotifyApiProperties;
import com.jorge.brewery.core.properties.IntegrationSpotifyApiUrlProperties;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Base64;

import static com.jorge.brewery.template.FixtureTemplateType.IDEAL;
import static com.jorge.brewery.template.FixtureTemplateType.VALID;
import static java.util.Collections.singletonList;
import static org.mockito.Mockito.*;

/**
 * Jorge Tavares jorge.tavares.inatel@gmail.com
 */
public class SpotifyRestClientImplTest extends AbstractTest {

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private IntegrationSpotifyApiUrlProperties spotifyApiUrlProperties;

    @Mock
    private IntegrationSpotifyApiProperties spotifyApiProperties;

    private SpotifyRestClient spotifyRestClient;

    @Override
    public void init() {
        spotifyRestClient = new SpotifyRestClientImpl(restTemplate, spotifyApiUrlProperties, spotifyApiProperties);
    }

    @Test
    public void should_retrieve_a_spotify_playlist_that_contains_dunkel_as_beer_style() {
        SpotifyGetPlaylistsResponse getPlaylistsResponse = Fixture.from(SpotifyGetPlaylistsResponse.class).gimme(IDEAL.name());
        SpotifyPlaylist spotifyPlaylist = getPlaylistsResponse.getPlaylists();
        SpotifyClientCredentialResponse credentialResponse = Fixture.from(SpotifyClientCredentialResponse.class).gimme(VALID.name());

        when(spotifyApiProperties.getClientId()).thenReturn("cf7e40ad5f0a4aa6be9f86a570423e36");
        when(spotifyApiProperties.getClientSecret()).thenReturn("509bbc8f0c1e42ce8ae5b291f26a235e");
        when(spotifyApiUrlProperties.getAuthorization()).thenReturn("https://accounts.spotify.com/api/token");

        HttpHeaders authHeaders = new HttpHeaders();
        authHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        authHeaders.set("Authorization", "Basic " + Base64.getEncoder().encodeToString("cf7e40ad5f0a4aa6be9f86a570423e36:509bbc8f0c1e42ce8ae5b291f26a235e".getBytes()));
        MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
        requestBody.add("grant_type", "client_credentials");
        final HttpEntity<MultiValueMap<String, String>> authHttpEntity = new HttpEntity<>(requestBody, authHeaders);
        when(restTemplate.exchange("https://accounts.spotify.com/api/token", HttpMethod.POST,
                authHttpEntity, SpotifyClientCredentialResponse.class)).thenReturn(ResponseEntity.ok(credentialResponse));
        when(spotifyApiUrlProperties.getSearch()).thenReturn("https://api.spotify.com/v1/search");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(singletonList(MediaType.APPLICATION_JSON));
        headers.set("Authorization", credentialResponse.getTokenType() + " " + credentialResponse.getAccessToken());
        final HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
        when(restTemplate.exchange("https://api.spotify.com/v1/search?q=Dunkel&type=playlist&limit=1&offset=0", HttpMethod.GET,
                httpEntity, SpotifyGetPlaylistsResponse.class)).thenReturn(ResponseEntity.ok(getPlaylistsResponse));

        Assert.assertEquals(spotifyPlaylist, spotifyRestClient.getPlaylistByName("Dunkel"));

        InOrder inOrder = inOrder(spotifyApiProperties, spotifyApiUrlProperties, restTemplate);
        inOrder.verify(spotifyApiProperties, times(1)).getClientId();
        inOrder.verify(spotifyApiProperties, times(1)).getClientSecret();
        inOrder.verify(spotifyApiUrlProperties, times(1)).getAuthorization();
        inOrder.verify(restTemplate, times(1)).exchange("https://accounts.spotify.com/api/token", HttpMethod.POST,
                authHttpEntity, SpotifyClientCredentialResponse.class);
        inOrder.verify(spotifyApiUrlProperties, times(1)).getSearch();
        inOrder.verify(restTemplate, times(1)).exchange("https://api.spotify.com/v1/search?q=Dunkel&type=playlist&limit=1&offset=0", HttpMethod.GET,
                httpEntity, SpotifyGetPlaylistsResponse.class);
        inOrder.verifyNoMoreInteractions();
    }

    @Test(expected = IntegrationException.class)
    public void should_throw_a_integration_exception_when_get_authorization_token_is_failure() {
        SpotifyGetPlaylistsResponse getPlaylistsResponse = Fixture.from(SpotifyGetPlaylistsResponse.class).gimme(IDEAL.name());
        SpotifyPlaylist spotifyPlaylist = getPlaylistsResponse.getPlaylists();

        when(spotifyApiProperties.getClientId()).thenReturn("cf7e40ad5f0a4aa6be9f86a570423e36");
        when(spotifyApiProperties.getClientSecret()).thenReturn("509bbc8f0c1e42ce8ae5b291f26a235e");
        when(spotifyApiUrlProperties.getAuthorization()).thenReturn("https://accounts.spotify.com/api/token");

        HttpHeaders authHeaders = new HttpHeaders();
        authHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        authHeaders.set("Authorization", "Basic " + Base64.getEncoder().encodeToString("cf7e40ad5f0a4aa6be9f86a570423e36:509bbc8f0c1e42ce8ae5b291f26a235e".getBytes()));
        MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
        requestBody.add("grant_type", "client_credentials");
        final HttpEntity<MultiValueMap<String, String>> authHttpEntity = new HttpEntity<>(requestBody, authHeaders);
        when(restTemplate.exchange("https://accounts.spotify.com/api/token", HttpMethod.POST,
                authHttpEntity, SpotifyClientCredentialResponse.class)).thenThrow(new RestClientException("..."));

        Assert.assertEquals(spotifyPlaylist, spotifyRestClient.getPlaylistByName("Dunkel"));

        InOrder inOrder = inOrder(spotifyApiProperties, spotifyApiUrlProperties, restTemplate);
        inOrder.verify(spotifyApiProperties, times(1)).getClientId();
        inOrder.verify(spotifyApiProperties, times(1)).getClientSecret();
        inOrder.verify(spotifyApiUrlProperties, times(1)).getAuthorization();
        inOrder.verify(restTemplate, times(1)).exchange("https://accounts.spotify.com/api/token", HttpMethod.POST,
                authHttpEntity, SpotifyClientCredentialResponse.class);
    }

    @Test(expected = IntegrationException.class)
    public void should_throw_a_integration_exception_when_request_is_failure() {
        SpotifyGetPlaylistsResponse getPlaylistsResponse = Fixture.from(SpotifyGetPlaylistsResponse.class).gimme(IDEAL.name());
        SpotifyPlaylist spotifyPlaylist = getPlaylistsResponse.getPlaylists();
        SpotifyClientCredentialResponse credentialResponse = Fixture.from(SpotifyClientCredentialResponse.class).gimme(VALID.name());

        when(spotifyApiProperties.getClientId()).thenReturn("cf7e40ad5f0a4aa6be9f86a570423e36");
        when(spotifyApiProperties.getClientSecret()).thenReturn("509bbc8f0c1e42ce8ae5b291f26a235e");
        when(spotifyApiUrlProperties.getAuthorization()).thenReturn("https://accounts.spotify.com/api/token");

        HttpHeaders authHeaders = new HttpHeaders();
        authHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        authHeaders.set("Authorization", "Basic " + Base64.getEncoder().encodeToString("cf7e40ad5f0a4aa6be9f86a570423e36:509bbc8f0c1e42ce8ae5b291f26a235e".getBytes()));
        MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
        requestBody.add("grant_type", "client_credentials");
        final HttpEntity<MultiValueMap<String, String>> authHttpEntity = new HttpEntity<>(requestBody, authHeaders);
        when(restTemplate.exchange("https://accounts.spotify.com/api/token", HttpMethod.POST,
                authHttpEntity, SpotifyClientCredentialResponse.class)).thenReturn(ResponseEntity.ok(credentialResponse));
        when(spotifyApiUrlProperties.getSearch()).thenReturn("https://api.spotify.com/v1/search");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(singletonList(MediaType.APPLICATION_JSON));
        headers.set("Authorization", credentialResponse.getTokenType() + " " + credentialResponse.getAccessToken());
        final HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
        when(restTemplate.exchange("https://api.spotify.com/v1/search?q=Dunkel&type=playlist&limit=1&offset=0", HttpMethod.GET,
                httpEntity, SpotifyGetPlaylistsResponse.class)).thenThrow(new RestClientException("..."));

        Assert.assertEquals(spotifyPlaylist, spotifyRestClient.getPlaylistByName("Dunkel"));

        InOrder inOrder = inOrder(spotifyApiProperties, spotifyApiUrlProperties, restTemplate);
        inOrder.verify(spotifyApiProperties, times(1)).getClientId();
        inOrder.verify(spotifyApiProperties, times(1)).getClientSecret();
        inOrder.verify(spotifyApiUrlProperties, times(1)).getAuthorization();
        inOrder.verify(restTemplate, times(1)).exchange("https://accounts.spotify.com/api/token", HttpMethod.POST,
                authHttpEntity, SpotifyClientCredentialResponse.class);
        inOrder.verify(spotifyApiUrlProperties, times(1)).getSearch();
        inOrder.verify(restTemplate, times(1)).exchange("https://api.spotify.com/v1/search?q=Dunkel&type=playlist&limit=1&offset=0", HttpMethod.GET,
                httpEntity, SpotifyGetPlaylistsResponse.class);
    }
}