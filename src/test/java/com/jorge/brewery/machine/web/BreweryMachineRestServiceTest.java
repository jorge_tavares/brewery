package com.jorge.brewery.machine.web;

import br.com.six2six.fixturefactory.Fixture;
import com.jorge.brewery.core.AbstractTest;
import com.jorge.brewery.machine.BreweryMachine;
import com.jorge.brewery.machine.IdealBeer;
import com.jorge.brewery.template.FixtureTemplateType;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

/**
 * Jorge Tavares jorge.tavares.inatel@gmail.com
 */
public class BreweryMachineRestServiceTest extends AbstractTest {

    @Mock
    private BreweryMachine breweryMachine;

    private BreweryMachineRestService breweryMachineRestService;

    @Override
    public void init() {
        breweryMachineRestService = new BreweryMachineRestService(breweryMachine);
    }

    @Test
    public void should_expose_the_ideal_beer_by_temperature() {
        IdealBeer idealBeer = Fixture.from(IdealBeer.class).gimme(FixtureTemplateType.IDEAL.name());

        when(breweryMachine.getIdealBeer(-2)).thenReturn(idealBeer);

        Assert.assertEquals(idealBeer, breweryMachineRestService.getIdeal(IdealTemperatureRequest.builder()
                .temperature(-2)
                .build()));

        InOrder inOrder = Mockito.inOrder(breweryMachine);
        inOrder.verify(breweryMachine, times(1)).getIdealBeer(-2);
        inOrder.verifyNoMoreInteractions();
    }
}