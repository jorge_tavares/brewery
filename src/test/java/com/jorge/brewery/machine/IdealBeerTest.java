package com.jorge.brewery.machine;

import br.com.six2six.fixturefactory.Fixture;
import com.jorge.brewery.core.AbstractTest;
import com.jorge.brewery.integration.spotify.SpotifyPlaylist;
import com.jorge.brewery.integration.spotify.SpotifyPlaylistItem;
import org.junit.Assert;
import org.junit.Test;

import static com.jorge.brewery.template.FixtureTemplateType.IDEAL;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
public class IdealBeerTest extends AbstractTest {

    private IdealBeer idealBeer;
    private SpotifyPlaylist spotifyPlaylist;

    @Override
    public void init() {
        idealBeer = Fixture.from(IdealBeer.class).gimme(IDEAL.name());
        spotifyPlaylist = Fixture.from(SpotifyPlaylist.class).gimme(IDEAL.name());
    }

    @Test
    public void should_create_a_new_of() {
        Assert.assertEquals(expected(idealBeer.getBeerStyle(), spotifyPlaylist), IdealBeer.of(idealBeer.getBeerStyle(), spotifyPlaylist));
    }

    private IdealBeer expected(String style, SpotifyPlaylist spotifyPlaylist) {
        final SpotifyPlaylistItem playlistItem = spotifyPlaylist.getItems().get(0);
        return IdealBeer.builder()
                .beerStyle(style)
                .playlist(IdealPlaylist.builder()
                        .self(spotifyPlaylist.getHref())
                        .name(playlistItem.getName())
                        .tracks(IdealTracks.builder()
                                .self(playlistItem.getTracks().getHref())
                                .musicTotal(playlistItem.getTracks().getTotal())
                                .build())
                        .build())
                .build();
    }
}