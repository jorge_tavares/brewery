package com.jorge.brewery.core;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
public class SwaggerConstant {
    public static final String HTTP_200_MSG = "Success";
    public static final String HTTP_201_MSG = "Created";
    public static final String HTTP_202_MSG = "Accepted";
    public static final String HTTP_204_MSG = "The server has fulfilled the request but does not need to return an entity-body.";
    public static final String HTTP_207_MSG = "The request finished with errors.";
    public static final String HTTP_400_MSG = "Request invalid or malformed.";
    public static final String HTTP_404_MSG = "Resource not found.";
    public static final String HTTP_401_MSG = "Unauthorized";
    public static final String HTTP_403_MSG = "Access Denied";
    public static final String HTTP_406_MSG = "Invalid content-type.";
    public static final String HTTP_409_MSG = "The identifier that already exists.";
    public static final String HTTP_500_MSG = "Internal Server or Business error.";

    private SwaggerConstant() {
    }
}
