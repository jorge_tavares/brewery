package com.jorge.brewery.core.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
@Data
@Component
@ConfigurationProperties(prefix = "integration.spotify.api.url")
public class IntegrationSpotifyApiUrlProperties {
    private String authorization;
    private String search;
}
