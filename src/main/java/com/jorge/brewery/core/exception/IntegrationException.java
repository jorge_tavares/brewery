package com.jorge.brewery.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class IntegrationException extends RuntimeException {

    public IntegrationException() {
        super();
    }
    public IntegrationException(String message, Throwable cause) {
        super(message, cause);
    }
    public IntegrationException(String message) {
        super(message);
    }
    public IntegrationException(Throwable cause) {
        super(cause);
    }
}
