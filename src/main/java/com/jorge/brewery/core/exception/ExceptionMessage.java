package com.jorge.brewery.core.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

import java.util.List;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
@Value
@Builder
@AllArgsConstructor
class ExceptionMessage {
    private ExceptionTypeMessage type;
    private String description;
    private List<String> notifications;
}
