package com.jorge.brewery.core.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
@Slf4j
@RestControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
class ErrorHandler {

    private static final String ERROR = "error";

    @ExceptionHandler(ResourceNotFoundException.class)
    public HttpEntity<ExceptionMessage> handlerResourceNotFoundException(final ExceptionMessage ex) {
        log.debug(ERROR, ex);
        HttpHeaders responseHeaders = new HttpHeaders();
        final ExceptionMessage message = ExceptionMessage.builder()
                .type(ExceptionTypeMessage.Resource_Not_Found)
                .description(ex.getDescription())
                .notifications(ex.getNotifications())
                .build();
        responseHeaders.add(CONTENT_TYPE, APPLICATION_JSON_UTF8_VALUE);
        return new ResponseEntity<>(message, responseHeaders, HttpStatus.NOT_FOUND);
    }


    @ExceptionHandler(AlreadyExistsException.class)
    public HttpEntity<ExceptionMessage> handlerAlreadyExistsException(final ExceptionMessage ex) {
        log.debug(ERROR, ex);
        HttpHeaders responseHeaders = new HttpHeaders();
        final ExceptionMessage message = ExceptionMessage.builder()
                .type(ExceptionTypeMessage.Conflict_Error)
                .description(ex.getDescription())
                .notifications(ex.getNotifications())
                .build();
        responseHeaders.add(CONTENT_TYPE, APPLICATION_JSON_UTF8_VALUE);
        return new ResponseEntity<>(message, responseHeaders, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(IntegrationException.class)
    public HttpEntity<ExceptionMessage> handlerIntegrationException(final ExceptionMessage ex) {
        log.debug(ERROR, ex);
        HttpHeaders responseHeaders = new HttpHeaders();
        final ExceptionMessage message = ExceptionMessage.builder()
                .type(ExceptionTypeMessage.Integration_Error)
                .description(ex.getDescription())
                .notifications(ex.getNotifications())
                .build();
        responseHeaders.add(CONTENT_TYPE, APPLICATION_JSON_UTF8_VALUE);
        return new ResponseEntity<>(message, responseHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
