package com.jorge.brewery.configuration;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
@Configuration
@EnableTransactionManagement
@EntityScan("com.jorge.brewery")
public class DomainConfiguration {
}
