package com.jorge.brewery.configuration;

import com.jorge.brewery.beer.Beer;
import com.jorge.brewery.beer.BeerService;
import lombok.AllArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import static com.google.common.collect.Lists.newArrayList;

@Configuration
@AllArgsConstructor
@Order(Ordered.HIGHEST_PRECEDENCE)
public class DatabasePopulationConfiguration implements ApplicationRunner {

    private final BeerService beerService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        populate();
    }

    private void populate() {
        newArrayList(createdBeer("Weissbier", -1, 3),
                createdBeer("Pilsen", -2, 4),
                createdBeer("Weizenbier", -4, 6),
                createdBeer("Red ale", -5, 5),
                createdBeer("India pale ale", -6, 7),
                createdBeer("IPA", -7, 10),
                createdBeer("Dunkel", -8, 2),
                createdBeer("Imperial Stouts", -10, 13),
                createdBeer("Brown ale", 0, 14))
                .forEach(beerService::create);
    }

    private Beer createdBeer(String style, Integer minTemperature, Integer maxTemperature) {
        return Beer.builder()
                .style(style)
                .minTemperature(minTemperature)
                .maxTemperature(maxTemperature)
                .build();
    }
}
