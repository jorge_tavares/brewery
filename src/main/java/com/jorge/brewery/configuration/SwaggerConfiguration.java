package com.jorge.brewery.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static java.util.Collections.emptyList;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
@Configuration
@EnableSwagger2
@PropertySource("classpath:swagger.properties")
public class SwaggerConfiguration {

    private static final String API_REST_SERVICES_PACKAGE = "com.jorge.brewery";
    private static final String API_NAME = "Brewery";
    private static final String API_DESCRIPTION = "API for Brewery ideal temperature consumes";
    private static final String API_VERSION = "v1";
    private static final String GROUP_NAME = "Jorge Brewery";

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(GROUP_NAME).select()
                .apis(RequestHandlerSelectors.basePackage(API_REST_SERVICES_PACKAGE))
                .paths(PathSelectors.any()).build().apiInfo(apiInfo())
                .useDefaultResponseMessages(false);
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(API_NAME, API_DESCRIPTION, API_VERSION, null,
                new Contact("Jorge Tavares", "", "jorge.tavares.inatel@gmail.com"),
                "", "", emptyList());
    }

}