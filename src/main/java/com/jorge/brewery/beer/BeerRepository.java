package com.jorge.brewery.beer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
@Repository
interface BeerRepository extends JpaRepository<Beer, Integer>, JpaSpecificationExecutor<Beer> {

    @Query(value = "SELECT TOP 1 * FROM Beer ORDER BY ABS(avg_temperature - (?1)) ASC, style ASC",
            nativeQuery = true)
    Optional<Beer> findIdeal(Integer temperature);
}
