package com.jorge.brewery.beer;

import com.jorge.brewery.core.exception.AlreadyExistsException;
import com.jorge.brewery.core.exception.ResourceNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.jorge.brewery.beer.BeerSpecification.id;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
@Service
@AllArgsConstructor
class BeerServiceImpl implements BeerService {

    private final BeerRepository repository;

    @Override
    public Beer create(Beer beer) {
        if (repository.findOne(id(beer.getId())).isPresent()) {
            throw new AlreadyExistsException("Beer ["+beer.getId()+"] already exists");
        }
        final Integer avgTemperature = averageBetween(beer.getMinTemperature(), beer.getMaxTemperature());
        return repository.save(beer.avgTemperature(avgTemperature));
    }

    @Override
    public Beer update(Integer id, Beer beer) {
        findOne(id(id));
        final Integer avgTemperature = averageBetween(beer.getMinTemperature(), beer.getMaxTemperature());
        return repository.save(beer.avgTemperature(avgTemperature));
    }

    @Override
    public Beer findOne(Specification<Beer> specification) {
        return repository.findOne(specification)
                .orElseThrow(() -> new ResourceNotFoundException("Beer not found"));
    }

    @Override
    public Page<Beer> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    @Override
    public void delete(Integer id) {
        repository.delete(findOne(id(id)));
    }

    @Override
    public Beer findIdeal(Integer temperature) {
        return repository.findIdeal(temperature)
                .orElseThrow(() -> new ResourceNotFoundException("Ideal Beer not found :("));
    }

    private Integer averageBetween(Integer num1, Integer num2) {
        return Math.addExact(num1, num2) / 2;
    }
}
