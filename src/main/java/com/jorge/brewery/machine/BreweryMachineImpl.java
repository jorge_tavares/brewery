package com.jorge.brewery.machine;

import com.jorge.brewery.beer.Beer;
import com.jorge.brewery.beer.BeerService;
import com.jorge.brewery.integration.spotify.SpotifyPlaylist;
import com.jorge.brewery.integration.spotify.SpotifyRestClient;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
@Service
@AllArgsConstructor
class BreweryMachineImpl implements BreweryMachine {

    private final BeerService beerService;
    private final SpotifyRestClient spotifyRestClient;

    @Override
    public IdealBeer getIdealBeer(Integer temperature) {
        final Beer ideal = beerService.findIdeal(temperature);
        final SpotifyPlaylist playlist = spotifyRestClient.getPlaylistByName(ideal.getStyle());
        return IdealBeer.of(ideal.getStyle(), playlist);
    }
}
