package com.jorge.brewery.machine;

import com.jorge.brewery.integration.spotify.SpotifyPlaylist;
import com.jorge.brewery.integration.spotify.SpotifyPlaylistItem;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
@Value
@Builder
@ToString
@EqualsAndHashCode
public class IdealBeer {

    private String beerStyle;
    private IdealPlaylist playlist;

    public static IdealBeer of(String style, SpotifyPlaylist playlist) {
        final SpotifyPlaylistItem playlistItem = playlist.getItems().get(0);
        return IdealBeer.builder()
                .beerStyle(style)
                .playlist(IdealPlaylist.builder()
                        .self(playlist.getHref())
                        .name(playlistItem.getName())
                        .tracks(IdealTracks.builder()
                                .self(playlistItem.getTracks().getHref())
                                .musicTotal(playlistItem.getTracks().getTotal())
                                .build())
                        .build())
                .build();
    }
}
