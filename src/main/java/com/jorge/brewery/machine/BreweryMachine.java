package com.jorge.brewery.machine;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
public interface BreweryMachine {
    IdealBeer getIdealBeer(Integer temperature);
}
